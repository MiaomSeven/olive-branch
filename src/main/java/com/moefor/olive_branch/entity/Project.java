package com.moefor.olive_branch.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @Column(nullable = false)
    private String title;

    @ManyToOne
    @JoinColumn
    private Student leader;

    @ManyToMany
    @JoinTable
    private Set<Student> member;

    @ManyToOne
    @JoinColumn
    private Teacher teacher;

    public enum State{
        ALLOW, FIN
    }
    @Enumerated
    @Column(nullable = false)
    private State state;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private Date createTime;


    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Student getLeader() {
        return leader;
    }

    public void setLeader(Student leader) {
        this.leader = leader;
    }

    public Set<Student> getMember() {
        return member;
    }

    public void setMember(Set<Student> member) {
        this.member = member;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
