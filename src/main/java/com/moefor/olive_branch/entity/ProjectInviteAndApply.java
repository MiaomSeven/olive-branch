package com.moefor.olive_branch.entity;

import javax.persistence.*;
import java.util.Date;

public class ProjectInvitationAndApply {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    public enum State{
        UNFIN, ALLOW, BAN
    }
    @Enumerated
    @Column(nullable = false)
    private State state;

    public enum Type{
        INVITATION, APPLY, INVITATION_LEADER
    }
    @Enumerated
    @Column(nullable = false)
    private Type type;

    @ManyToOne
    @JoinColumn
    private Project project;

    @ManyToOne
    @JoinColumn
    private Student student;

    @Column
    private Date time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
