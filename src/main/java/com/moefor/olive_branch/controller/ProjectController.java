package com.moefor.olive_branch.controller;

import com.moefor.olive_branch.entity.Project;
import com.moefor.olive_branch.entity.User;
import com.moefor.olive_branch.service.ProjectService;
import com.moefor.olive_branch.util.SimpleVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping(value = {"/project"})
public class ProjectController {
    @Autowired
    private ProjectService projectService;

    @Value("${app.porject-page-size}")
    private Integer projectPageSize;

    @GetMapping(value = {"","/"})
    public ModelAndView index(
            @RequestParam(value = "page_num", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "page_size", required = false) Integer pageSize
    )throws Exception{
        ModelAndView modelAndView = new ModelAndView("/project/index");
        if(pageSize == null) pageSize = projectPageSize;
        Page<Project> projectPage = projectService.getRecentProject(pageNum, pageSize);
        modelAndView.addObject("projectPage", projectPage);
        return modelAndView;
    }

    @GetMapping(value = {"/detail/{id}"}, produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView detail(
            @PathVariable("id") Integer id
    ) throws Exception{
        ModelAndView modelAndView = new ModelAndView("/project/detail");
        Project project = projectService.getById(id);
        modelAndView.addObject("/project/detail");
        return modelAndView;
    }

    @GetMapping(value = {"search"})
    public ModelAndView search(
            @RequestParam("title") String title,
            @RequestParam(value = "page_num", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "page_size", required = false) Integer pageSize
    )throws Exception{
        if (pageSize ==  null) pageSize = projectPageSize;
        List<String> titleList = Arrays.asList(StringUtils.split(title, " "));
        Page<Project> projectPage = projectService.searchRecentProject(titleList, pageNum, pageSize);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/project/search");
        modelAndView.addObject("projectPage", projectPage);
        return modelAndView;
    }

    @ResponseBody
    @GetMapping(value = {"/{id}"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Project getById(
            @PathVariable("id") Integer id
    )throws Exception{
        return projectService.getById(id);
    }


    @ResponseBody
    @GetMapping(value = {"/list"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ModelAndView list(
            @RequestParam(value = "page_num", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "page_size", required = false) Integer pageSize
    )throws Exception{
        if (pageSize ==  null) pageSize = projectPageSize;
        Page<Project> projectPage = projectService.getRecentProject(pageNum, pageSize);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/project/list");
        modelAndView.addObject("projectPage", projectPage);
        return modelAndView;
    }

    @ResponseBody
    @PutMapping(value = {""}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public SimpleVo add(
            @RequestParam("title") String titile,
            @RequestParam("description") String description,
            @SessionAttribute(value = "user", required = false) User user
    )throws Exception{
        if(ObjectUtils.isEmpty(user)) throw new Exception("用户未登录，权限不足。");
        Project project = null;
        if(!ObjectUtils.isEmpty(user.getStudent())){
            project = projectService.addProjectByStudent(titile, description,user.getStudent().getId());
        } else if (!ObjectUtils.isEmpty(user.getTeacher())){
            project = projectService.addProjectByStudent(titile, description,user.getTeacher().getId());
        } else {
            throw new Exception("用户角色不明，权限不足");
        }
        return new SimpleVo(project, "创建项目成功");
    }

    @ResponseBody
    @PostMapping(value = {""})
    public SimpleVo inviteMember(
            @RequestParam("invite_")
    ){
        return null;
    }
}
