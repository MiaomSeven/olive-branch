package com.moefor.olive_branch.controller;

import com.moefor.olive_branch.entity.Student;
import com.moefor.olive_branch.entity.User;
import com.moefor.olive_branch.service.UserService;
import com.moefor.olive_branch.util.SimpleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;


@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = {"", "/", "/index"})
    public ModelAndView index(
            @SessionAttribute(value = "user", required = false) User user
    )throws Exception{
        ModelAndView modelAndView = new ModelAndView();
        if(ObjectUtils.isEmpty(user)){
            modelAndView.setViewName("redirect:/user/signin");
        }else {
            modelAndView.setViewName("/user/index");
            modelAndView.addObject("user", user);
        }
        return modelAndView;
    }

    @ResponseBody
    @PostMapping(value = {"/", ""})
    public SimpleVo signin(
            HttpSession httpSession,
            @SessionAttribute(value = "user", required = false) User user,
            @RequestParam("username") String username,
            @RequestParam("password")  String password
    )throws Exception{
        if(!ObjectUtils.isEmpty(user)) throw new Exception();
        Assert.hasText(username, "用户名不能为空");
        Assert.hasText(password, "密码不能为空");
        user = userService.login(username.toLowerCase(), password);
        user.setPassword(null);
        httpSession.setAttribute("user", user);
        return new SimpleVo(user, "用户登录成功", HttpStatus.OK);
    }

    @ResponseBody
    @PutMapping(value = {"/", ""})
    public SimpleVo signup(
            @SessionAttribute(value = "user", required = false) User user,
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            @RequestParam("repassword") String repassword
    )throws Exception{
        if(!ObjectUtils.isEmpty(user)) throw new Exception("用户已登陆");
        Assert.hasText(username, "用户名不能为空");
        Assert.hasText(password, "密码不能为空");
        if(!repassword.equals(password)) throw new Exception("两次密码不相同");
        user = userService.add(username.toLowerCase(), password);
        user.setPassword(null);
        SimpleVo simpleVo = new SimpleVo(user, "用户注册成功");
        return simpleVo;
    }

    @GetMapping(value = {"/signin"})
    public ModelAndView signinView(
            @SessionAttribute(value = "user", required = false) User user
    )throws Exception{
        ModelAndView modelAndView = new ModelAndView();
        if(!ObjectUtils.isEmpty(user)){
            modelAndView.setViewName("redirect:/user/");
        }else {
            modelAndView.setViewName("/user/signin");
        }
        return modelAndView;
    }

    @GetMapping(value = {"/signup"})
    public ModelAndView signupView(
            @SessionAttribute(value = "user", required = false) User user
    )throws Exception{
        ModelAndView modelAndView = new ModelAndView();
        if(!ObjectUtils.isEmpty(user)){
            modelAndView.setViewName("redirect:/user/");
        }else {
            modelAndView.setViewName("/user/signup");
        }
        return modelAndView;
    }

    @ResponseBody
    @PutMapping(value = {"/student"})
    public SimpleVo updToStudent(
            @SessionAttribute(value = "user", required = false) User user,
            @RequestParam("no") String no,
            @RequestParam("college") String college,
            @RequestParam("professional") String professional,
            @RequestParam("class") String clazz
    )throws Exception{
        if(ObjectUtils.isEmpty(user)) throw new Exception("用户wei登陆");
        Assert.hasText(no, "");
        Assert.hasText(college, "");
        Assert.hasText(professional,"");
        Assert.hasText(clazz,"");
        Student student = userService.updToStudent(user.getId(), no, college, professional, clazz);
        return new SimpleVo(student, "");
    }
}
