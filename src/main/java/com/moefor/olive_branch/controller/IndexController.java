package com.moefor.olive_branch.controller;

import com.moefor.olive_branch.entity.Project;
import com.moefor.olive_branch.service.ProjectService;
import com.moefor.olive_branch.util.DefaultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = {"", "/"})
public class IndexController {

    @Autowired
    private ProjectService projectService;

    @Value("${app.porject-page-size}")
    private Integer projectPageSize;

    @GetMapping(value = {"", "/", "index"})
    public ModelAndView index(
            @RequestParam(value = "page_num", defaultValue = "1") Integer pageNum,
            @RequestParam(value = "page_size", required = false) Integer pageSize
    ) throws Exception{
        ModelAndView modelAndView = new ModelAndView("index");
        pageSize = DefaultUtil.integerDefault(null, projectPageSize);
        modelAndView.addObject("projectPage", projectService.getRecentProject(pageNum, pageSize));
        return modelAndView;
    }
}
