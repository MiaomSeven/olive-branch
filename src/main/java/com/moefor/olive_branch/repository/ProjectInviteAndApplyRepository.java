package com.moefor.olive_branch.repository;

import com.moefor.olive_branch.entity.ProjectInvitationAndApply;
import com.moefor.olive_branch.entity.Teacher;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectInvitationAndApplyRepository extends CrudRepository<ProjectInvitationAndApply, Integer> {

}
