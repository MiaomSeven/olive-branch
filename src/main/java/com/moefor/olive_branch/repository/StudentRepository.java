package com.moefor.olive_branch.repository;

import com.moefor.olive_branch.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CrudRepository<Student, Integer> {

    boolean existsByIdAndProject_Id(int id, int projectId);
}
