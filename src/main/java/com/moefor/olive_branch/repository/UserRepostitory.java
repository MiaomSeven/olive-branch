package com.moefor.olive_branch.repository;

import com.moefor.olive_branch.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepostitory extends CrudRepository<User, Integer> {

    User findByUsername(String username);
    boolean existsByUsername(String username);
}
