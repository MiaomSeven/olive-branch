package com.moefor.olive_branch.service;

import com.moefor.olive_branch.entity.Project;
import com.moefor.olive_branch.entity.ProjectMemberLog;
import com.moefor.olive_branch.entity.Student;
import com.moefor.olive_branch.entity.User;
import com.moefor.olive_branch.repository.ProjectRepository;
import com.moefor.olive_branch.repository.ProjetMemberLogRepository;
import com.moefor.olive_branch.repository.StudentRepository;
import com.moefor.olive_branch.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;

import java.util.Date;
import java.util.List;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private ProjetMemberLogRepository projetMemberLogRepository;

    @Transactional(readOnly = true)
    public Page<Project> searchRecentProject(List<String> titleList, int pageNum, int pageSize) throws Exception{
        PageRequest pageRequest = new PageRequest(pageNum-1, pageSize);
        Page<Project> projectPage = projectRepository.findAllByTitleLikeOrderByCreateTimeDesc(titleList, pageRequest);
        return projectPage;
    }

    @Transactional(readOnly = true)
    public Page<Project> getRecentProject(int pageNum, int pageSize) throws Exception{
        Pageable pageable = new PageRequest(pageNum-1, pageSize);
        Page<Project> projectPage = projectRepository.findAllByOrderByCreateTimeDesc(pageable);
        return projectPage;
    }

    @Transactional(readOnly = true)
    public Project getById(int id) throws Exception{
        Project project = projectRepository.findOne(id);
        if(ObjectUtils.isEmpty(project)) throw new Exception();
        return project;
    }

    @Transactional
    public Project addProjectByStudent(String title, String description, int studentId) throws Exception{
        Project project = new Project();
        project.setTitle(title);
        project.setDescription(description);
        project.setCreateTime(new Date());
        project.setLeader(studentRepository.findOne(studentId));
        project.setState(Project.State.ALLOW);
        if(null != projectRepository.save(project)) return  project;
        throw new Exception("数据库错误");
    }

    @Transactional
    public Project addProjectByTeacher(String title, String description, int teacherId) throws Exception{
        Project project = new Project();
        project.setTitle(title);
        project.setDescription(description);
        project.setCreateTime(new Date());
        project.setTeacher(teacherRepository.findOne(teacherId));
        project.setState(Project.State.ALLOW);
        if(null != projectRepository.save(project)) return  project;
        throw new Exception("数据库错误");
    }

    @Transactional
    public boolean inviteMember(int projectId, int leaderId, int targetStudentId) throws Exception{
        Project project = projectRepository.findOne(projectId);
        Assert.notNull(project, "项目不存在");
        Student leader = studentRepository.findOne(leaderId);
        if(!project.getLeader().equals(leader)) throw new Exception("您非项目组长，无权邀请成员");
        if (project.getState() != Project.State.ALLOW) throw new Exception("项目已经完结");
        Student targetStudent = studentRepository.findOne(targetStudentId);
        Assert.notNull(targetStudent, "受邀学生不存在");
        ProjectMemberLog projectMemberLog = new ProjectMemberLog();
        projectMemberLog.setProject(project);
        projectMemberLog.setTarget(targetStudent);
        projectMemberLog.setState(ProjectMemberLog.State.UNFIN);
        projectMemberLog.setType(ProjectMemberLog.Type.INVITATION);
        return null != projetMemberLogRepository.save(projectMemberLog);
    }

    public boolean inviteLeader(int projectId, int teacherId, int tagetLeaderId) throws Exception{
        Project project = projectRepository.findOne(projectId);
        Assert.notNull(project, "项目不存在");
        Assert.notNull(project.getTeacher(), "");
        if(project.getTeacher().getId() != teacherId) throw new Exception("");
        Assert.notNull(project.getLeader(), "");
        if(project.getLeader().getId() == tagetLeaderId) throw new Exception("");
        Student leader = studentRepository.findOne(tagetLeaderId);
    }

    @Transactional
    public boolean applyForProject(int projectId, int studentId) throws Exception{
        Student student = studentRepository.findOne(studentId);
        Project project = projectRepository.findOne(projectId);
        Assert.notNull(project, "项目不存在");
        if (project.getState() != Project.State.ALLOW) throw new Exception("项目已经完结");
        ProjectMemberLog projectMemberLog = new ProjectMemberLog();
        projectMemberLog.setProject(project);
        projectMemberLog.setTarget(project.getLeader());
        projectMemberLog.setState(ProjectMemberLog.State.UNFIN);
        projectMemberLog.setType(ProjectMemberLog.Type.APPLY);
        return null != projetMemberLogRepository.save(projectMemberLog);
    }

}
