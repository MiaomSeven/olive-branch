package com.moefor.olive_branch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OliveBranchApplication {
    public static void main(String[] args){
        SpringApplication.run(OliveBranchApplication.class, args);
    }
}
